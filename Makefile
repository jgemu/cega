SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

NAME := cega
JGNAME := $(NAME)

SRCDIR := $(SOURCEDIR)/src

INCLUDES = -I$(SRCDIR)/emu2413 -I$(SRCDIR)/m68k -I$(SRCDIR)/ymfm
INCLUDES_JG = -I$(SRCDIR)

LINKER = $(CC)

LIBS = -lm
LIBS_STATIC =

LIBS_REQUIRES := speexdsp

DOCS := LICENSE README

# Object dirs
MKDIRS := emu2413 m68k ymfm

override INSTALL_DATA := 0
override INSTALL_EXAMPLE := 0
override INSTALL_SHARED := 0

include $(SOURCEDIR)/version.h
include $(SOURCEDIR)/mk/jg.mk

INCLUDES += $(CFLAGS_SPEEXDSP)
LIBS += $(LIBS_SPEEXDSP)

EXT := c
FLAGS := -std=c11 $(WARNINGS_DEF_C)

CSRCS := emu2413/emu2413.c \
	m68k/m68kcpu.c \
	m68k/m68kops.c \
	ymfm/ymfm_adpcm.c \
	ymfm/ymfm_opn.c \
	ymfm/ymfm_shim.c \
	ymfm/ymfm_ssg.c \
	eep24cxx.c \
	eep93c46.c \
	cega.c \
	cega_db.c \
	cega_m68k.c \
	cega_md.c \
	cega_md_io.c \
	cega_mixer.c \
	cega_serial.c \
	cega_sg.c \
	cega_sms.c \
	cega_sms_io.c \
	cega_z80.c \
	mdvdp.c \
	smsvdp.c \
	sn76496.c \
	ym2413.c \
	z80.c

JGSRCS := jg.c

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o)) $(OBJS_SPEEXDSP)
OBJS_JG := $(patsubst %,$(OBJDIR)/%,$(JGSRCS:.c=.o))

# Core commands
BUILD_JG = $(call COMPILE_C, $(FLAGS) $(INCLUDES_JG) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_C, $(FLAGS) $(INCLUDES))

.PHONY: $(PHONY)

all: $(TARGET)

# Rules
$(OBJDIR)/%.o: $(SRCDIR)/%.c $(PREREQ)
	$(call COMPILE_INFO,$(BUILD_MAIN))
	@$(BUILD_MAIN)

# Data rules
install-docs::
	cp $(SRCDIR)/emu2413/LICENSE $(DESTDIR)$(DOCDIR)/LICENSE-emu2413

include $(SOURCEDIR)/mk/rules.mk
