/*
 * Copyright (c) 2021 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_MD_IO_H
#define CEGA_MD_IO_H

typedef struct _md_ioport_t {
    uint8_t data;
    uint8_t ctrl;
    uint8_t txdata;
    uint8_t rxdata;
    uint8_t sctrl;
    uint8_t step;
    uint64_t cyc; // Keep track of cycles when polled
} md_ioport_t;

#define MD_INPUT_TYPE_NONE  0
#define MD_INPUT_TYPE_PAD3B 1
#define MD_INPUT_TYPE_PAD6B 2

#define MD_INPUT_U 0x0001
#define MD_INPUT_D 0x0002
#define MD_INPUT_L 0x0004
#define MD_INPUT_R 0x0008
#define MD_INPUT_M 0x0010
#define MD_INPUT_S 0x0020
#define MD_INPUT_A 0x0040
#define MD_INPUT_B 0x0080
#define MD_INPUT_C 0x0100
#define MD_INPUT_X 0x0200
#define MD_INPUT_Y 0x0400
#define MD_INPUT_Z 0x0800

uint8_t cega_md_io_rd(uint32_t);
void cega_md_io_wr(uint32_t, uint8_t);
void cega_md_io_set_port(int, int);
void cega_md_io_init(void);

#endif
