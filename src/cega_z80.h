/*
 * Copyright (c) 2021-2024 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_Z80_H
#define CEGA_Z80_H

#define NUMZ80 2

void cega_z80_init(unsigned);
void cega_z80_irq(unsigned, uint8_t);
void cega_z80_irq_clr(unsigned);
uint8_t cega_z80_busack(unsigned);
void cega_z80_busreq(unsigned, uint8_t);
void cega_z80_nmi(unsigned);
void cega_z80_nmi_clr(unsigned);
void cega_z80_reset(unsigned);
uint8_t cega_z80_reset_stat(unsigned);
void cega_z80_reset_line(unsigned, uint8_t);
void cega_z80_delay(unsigned, uint64_t);
void cega_z80_exec(unsigned);

void cega_z80_state_load(unsigned, uint8_t*);
void cega_z80_state_save(unsigned, uint8_t*);

extern uint8_t (*cega_z80_port_rd[NUMZ80])(uint8_t);
extern void (*cega_z80_port_wr[NUMZ80])(uint8_t, uint8_t);
extern uint8_t (*cega_z80_mem_rd[NUMZ80])(uint16_t);
extern void (*cega_z80_mem_wr[NUMZ80])(uint16_t, uint8_t);

#endif
