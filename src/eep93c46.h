/*
Copyright (c) 2021-2025 Rupert Carmichael
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EEP93C46_H
#define EEP93C46_H

#define EEP93C46_MODE_STANDBY   0x00
#define EEP93C46_MODE_START     0x01
#define EEP93C46_MODE_OPCODE    0x02
#define EEP93C46_MODE_WRAL      0x03
#define EEP93C46_MODE_WRITE     0x04
#define EEP93C46_MODE_READ      0x05

typedef struct _eep93c46_t {
    uint8_t clk;
    uint8_t cs;
    uint8_t cyc;
    uint8_t dataout;
    uint8_t opcode;
    uint8_t addr;
    uint8_t mode;
    uint8_t wrenable;
    uint16_t datain;
} eep93c46_t;

void eep93c46_init(eep93c46_t* const);
uint8_t eep93c46_rd(eep93c46_t* const);
void eep93c46_wr(eep93c46_t* const, uint8_t*, uint8_t);

#endif
